# needs work and commenting

# install.packages("jsonlite")
library("jsonlite")

#data <- unlist(jsonlite::fromJSON("https://raw.githubusercontent.com/smitty0015/OCSWinter2020/master/R/analysis/short_exp_test_data.json"), recursive = FALSE, use.names = TRUE)
data <- unlist(jsonlite::fromJSON("./data/data.json"), recursive = FALSE, use.names = TRUE)


# install.packages("plyr")
library("plyr")

data <- rbind.fill(data)


extract_json <- function(key, survey_data){
  return(survey_data[[max(grep(key, survey_data))]])
}

# extract_subject_data <- function(d){
#   
#   # extract_json <- function(key, survey_data){
#   #   return(survey_data[[max(grep(key, survey_data))]])
#   # }
#   
#   # survey_data <- d$responses[d$phase == "Survey"]
#   # survey_data <- survey_data[! is.na(survey_data)]
#   # survey_data <- sapply(survey_data, fromJSON)
#   
#   data.frame(d$subject,
#              #mean(d$correct[d$phase == "Sex"], na.rm=TRUE),
#              #mean(d$correct[d$phase == "Language"], na.rm=TRUE),
#              #mean(d$correct[d$phase == "Age"], na.rm=TRUE),
#              #max(d$time_elapsed) / 60000,
#              as.numeric(extract_json("Childcare", survey_data)),
#              as.numeric(extract_json("Caregiver", survey_data)),
#              as.numeric(extract_json("Par_Age", survey_data)),
#              as.character(extract_json("Gender_Q", survey_data)[[1]]),
#              as.character(extract_json("Gender_Q", survey_data)[[2]]),
#              as.character(extract_json("Country_Q", survey_data)[[1]]),
#              as.character(extract_json("Country_Q", survey_data)[[2]]),
#              as.character(extract_json("Normal_hearing", survey_data)),
#              as.character(extract_json("Engl_first_lang", survey_data)),
#              as.character(paste0(extract_json("Know_corp_lang", survey_data))),
#              as.character(extract_json("monolingual", survey_data)),
#              stringsAsFactors = FALSE)
#   
# }

tidy_data <- data.frame(matrix(0, length(unique(data$subject))*34, 15))
count<-1
for(i in 1:length(unique(data$subject))){
  sub_data <- data[data$subject == unique(data$subject)[i],]
  survey_data <- sub_data$responses[sub_data$phase == "Survey"]
  survey_data <- survey_data[! is.na(survey_data)]
  survey_data <- sapply(survey_data, fromJSON)
  subject<-sub_data$subject[1]
  childcare<-as.numeric(extract_json("Childcare", survey_data))
  caregiver<-as.numeric(extract_json("Caregiver", survey_data))
  par_age<-as.numeric(extract_json("Par_Age", survey_data))
  par_gen<-as.character(extract_json("Gender_Q", survey_data)[[1]])
  par_gen_other<-as.character(extract_json("Gender_Q", survey_data)[[2]])
  par_country<-as.character(extract_json("Country_Q", survey_data)[[1]])
  par_country_other<-as.character(extract_json("Country_Q", survey_data)[[2]])
  hearing<-as.character(extract_json("Normal_hearing", survey_data))
  engl_first<-as.character(extract_json("Engl_first_lang", survey_data))
  know_corp<-as.character(paste0(extract_json("Know_corp_lang", survey_data)))
  monolingual<-as.character(extract_json("monolingual", survey_data))
  for(j in 1:nrow(sub_data)){
    if(sub_data$correct[j] %in% c(0,1)){
      tidy_data[count,] <- data.frame(subject,
                                  sub_data$correct[j],
                                  sub_data$phase[j],
                                  sub_data$time_elapsed[j]/60000,
                                  childcare,
                                  caregiver,
                                  par_age,
                                  par_gen,
                                  par_gen_other,
                                  par_country,
                                  par_country_other,
                                  hearing,
                                  engl_first,
                                  know_corp,
                                  monolingual,
                                  stringsAsFactors = FALSE)
      count<-count+1
    }
  }
  #tidy_data[i, ] <- extract_subject_data(data[data$subject == unique(data$subject)[i], ])
  print(count)
}

colnames(tidy_data) <- c("subject_ID", "correct", "phase", "time_ellapsed_mins", "childcare", "caregiver", 
                         "age", "gender", "gender_text", "country", "country_text", "hearing", 
                         "eng_first", "know_corp_lang", "monolingual")

library(lme4)
model<- glmer(correct~childcare+caregiver+gender+(1|subject_ID),
              data = subset(tidy_data,
                            tidy_data$gender%in%c("Male","Female") & tidy_data$phase=="Sex"),
              family = binomial(link=logit))

summary(model)

#write.csv(tidy_data, "./data/cleaned_data.csv", row.names = FALSE)

